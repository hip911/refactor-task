<?php
namespace Refactor\Application\Controller;

use League\Fractal\Manager;
use League\Fractal\Resource\ResourceInterface;
use Psr\Http\Message\ServerRequestInterface;
use Refactor\Application\Exception\ResourceNotFound;
use Refactor\Application\Repository\UserRepository;
use Refactor\Common\Controller\AbstractRestfulController;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class ApplicationController
 * @package Refactor\Application\Controller
 */
class ApplicationController extends AbstractRestfulController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var Manager
     */
    private $manager;

    /**
     * ApplicationController constructor.
     * @param UserRepository $userRepository
     * @param Manager $manager
     */
    public function __construct(UserRepository $userRepository, Manager $manager)
    {
        $this->userRepository = $userRepository;
        $this->manager = $manager;
    }

    /**
     * Get a resource
     * @param ServerRequestInterface $request
     * @throws ResourceNotFound
     * @return string
     */
    public function get(ServerRequestInterface $request){
        $user = $this->userRepository->getById($request->getAttribute('id'));
        if(!$user) {
            throw new ResourceNotFound('Unable to find user');
        }

        return $this->sendJsonResponse($this->transformData($user));
    }

    /**
     * Get a list of resources
     * @param ServerRequestInterface $request
     * @return string
     */
    public function getList(ServerRequestInterface $request){
        $users = $this->userRepository->getAll();

        return $this->sendJsonResponse($this->transformData($users));
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    private function sendJsonResponse($data)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Content-Type' => 'json',
        ];
        $response = new JsonResponse($data, 200, $headers);

        return $response;
    }

    /**
     * @param ResourceInterface $resource
     * @return array
     */
    private function transformData($resource)
    {
        return $this->manager->createData($resource)->toArray();
    }
}