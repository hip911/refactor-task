<?php

namespace Refactor\Application\Transformer;

use League\Fractal\TransformerAbstract;
use Refactor\Application\Model\User;

class UserTransformer extends TransformerAbstract
{
    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'secondName' => $user->getSecondName(),
        ];
    }
}