<?php

namespace Refactor\Application\Repository;

use League\Csv\Reader as CsvReader;
use Refactor\Application\Factory\CsvReaderFactory;
use Refactor\Application\Factory\ResourceFactory;
use Refactor\Application\Factory\UserFactory;

class CsvUserRepository implements UserRepository
{
    /** @var CsvReader */
    private $csvReader;

    private $factory;

    private $resourceFactory;

    public function __construct(CsvReaderFactory $csvReaderFactory, UserFactory $factory, ResourceFactory $resourceFactory)
    {
        $this->csvReader = $csvReaderFactory->make();
        $this->factory = $factory;
        $this->resourceFactory = $resourceFactory;
    }

    public function getAll()
    {
        $users = [];
        foreach ($this->csvReader as $index => $row) {
            $users[] = $this->factory->createFromCsvRow($row);
        }

        return $this->resourceFactory->createFor($users);
    }

    public function getById($id)
    {
        foreach ($this->csvReader as $index => $row) {
            if ($row[0] !== $id) {
                continue;
            }

            return $this->resourceFactory->createFor($this->factory->createFromCsvRow($row));
        }

        return false;
    }
}