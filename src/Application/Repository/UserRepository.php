<?php

namespace Refactor\Application\Repository;

interface UserRepository
{
    public function getAll();

    public function getById($id);
}