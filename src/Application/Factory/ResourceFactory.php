<?php

namespace Refactor\Application\Factory;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ResourceFactory
{
    /**
     * @param $resource
     * @return Collection|Item
     */
    public function createFor($resource)
    {
        if(is_array($resource)) {
            $transformerName = $this->getTransformerFor($resource[0]);
            return new Collection($resource,new $transformerName);
        } else {
            $transformerName = $this->getTransformerFor($resource);
            return new Item($resource, new $transformerName);
        }
    }

    /**
     * @param $resource
     * @return string
     */
    private function getTransformerFor($resource)
    {
        $fqcn = get_class($resource);
        $parts = explode('\\',$fqcn);
        $className = $parts[count($parts) - 1];
        return 'Refactor\Application\Transformer\\'.$className.'Transformer';
    }
}