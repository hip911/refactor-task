<?php

namespace Refactor\Application\Factory;

use League\Csv\Reader;

class CsvReaderFactory
{
    /**
     * @var Reader
     */
    private $reader;

    /**
     * CsvReaderFactory constructor.
     * @param $csvPath
     */
    public function __construct($csvPath)
    {
        $this->reader = Reader::createFromPath($csvPath,'r');
    }

    /**
     * @return Reader
     */
    public function make()
    {
        return $this->reader;
    }
}