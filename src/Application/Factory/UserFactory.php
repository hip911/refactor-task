<?php

namespace Refactor\Application\Factory;

use Refactor\Application\Model\User;
use Zend\Hydrator\Reflection;

class UserFactory
{
    /**
     * @var Reflection
     */
    private $hydrator;

    /**
     * UserFactory constructor.
     * @param Reflection $hydrator
     */
    public function __construct(Reflection $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * @param $row
     * @return object
     */
    public function createFromCsvRow($row)
    {
        $data = ['id' => $row[0],'firstName'=>$row[1],'secondName'=>$row[2]];
        $user = new User;

        return $this->hydrator->hydrate($data,$user);
    }
}