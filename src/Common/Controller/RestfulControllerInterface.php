<?php
namespace Refactor\Common\Controller;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface RestfulControllerInterface
 * @package Refactor\Common\Controller
 */
interface RestfulControllerInterface
{
    /**
     * Get a resource
     * @param ServerRequestInterface $request
     * @return array
     */
    public function get(ServerRequestInterface $request);

    /**
     * Get a list of resources
     * @param ServerRequestInterface $request
     * @return mixed
     */
    public function getList(ServerRequestInterface $request);
}