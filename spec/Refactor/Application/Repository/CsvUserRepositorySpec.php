<?php

namespace spec\Refactor\Application\Repository;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Prophecy\Argument;
use Refactor\Application\Factory\CsvReaderFactory;
use Refactor\Application\Factory\ResourceFactory;
use Refactor\Application\Factory\UserFactory;
use Refactor\Application\Model\User;
use Refactor\Application\Repository\CsvUserRepository;
use PhpSpec\ObjectBehavior;

class CsvUserRepositorySpec extends ObjectBehavior
{
    private $csvReader;
    private $userFactory;
    private $resourceFactory;

    function let(CsvReaderFactory $csvReader, UserFactory $userFactory, ResourceFactory $resourceFactory)
    {
        $this->csvReader = $csvReader;
        $this->userFactory = $userFactory;
        $this->resourceFactory = $resourceFactory;
        $this->beConstructedWith($csvReader, $userFactory, $resourceFactory);
        $this->csvReader->make()->willReturn([[999],[998]]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CsvUserRepository::class);
    }

    function it_returns_all_users_in_a_collection(Collection $collection)
    {
        $this->resourceFactory->createFor(Argument::type('array'))->willReturn($collection);
        $this->getAll()->shouldReturnAnInstanceOf(Collection::class);
        $this->userFactory->createFromCsvRow(Argument::type('array'))->shouldHaveBeenCalledTimes(2);
    }

    function it_returns_a_single_user_in_a_resource_item(User $user, Item $item)
    {
        $this->userFactory->createFromCsvRow(Argument::type('array'))->willReturn($user);
        $this->resourceFactory->createFor(Argument::type(User::class))->willReturn($item);
        $this->getById(999)->shouldReturnAnInstanceOf(Item::class);
    }
}
