<?php

namespace spec\Refactor\Application\Factory;

use League\Csv\Reader;
use Refactor\Application\Factory\CsvReaderFactory;
use PhpSpec\ObjectBehavior;

class CsvReaderFactorySpec extends ObjectBehavior
{
    private $reader;

    function let()
    {
        $csvPath = '/tmp/path';
        $this->reader = Reader::createFromPath($csvPath,'r');
        $this->beConstructedWith($csvPath);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CsvReaderFactory::class);
    }

    function it_makes_the_reader()
    {
        $this->make()->shouldReturnAnInstanceOf($this->reader);
    }
}
