<?php

namespace spec\Refactor\Application\Factory;

use Prophecy\Argument;
use Refactor\Application\Factory\UserFactory;
use PhpSpec\ObjectBehavior;
use Refactor\Application\Model\User;
use Zend\Hydrator\Reflection;

class UserFactorySpec extends ObjectBehavior
{
    private $hydrator;

    function let(Reflection $hydrator)
    {
        $this->hydrator = $hydrator;
        $this->beConstructedWith($this->hydrator);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(UserFactory::class);
    }

    function it_creates_a_user_from_raw_data(User $hydratedUser)
    {
        $row = [999,'John','Doe'];
        $data = ['id' => $row[0],'firstName'=>$row[1],'secondName'=>$row[2]];
        $user = new User;

        $this->hydrator->hydrate($data,$user)->shouldBeCalled();
        $this->createFromCsvRow($row);
    }
}
