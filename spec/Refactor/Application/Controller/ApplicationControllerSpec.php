<?php

namespace spec\Refactor\Application\Controller;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Scope;
use Psr\Http\Message\ServerRequestInterface;
use Refactor\Application\Controller\ApplicationController;
use PhpSpec\ObjectBehavior;
use Refactor\Application\Exception\ResourceNotFound;
use Refactor\Application\Repository\UserRepository;
use Zend\Diactoros\Response\JsonResponse;

class ApplicationControllerSpec extends ObjectBehavior
{
    private $userRepository;
    private $manager;

    function let(UserRepository $userRepository, Manager $manager)
    {
        $this->userRepository = $userRepository;
        $this->manager = $manager;
        $this->beConstructedWith($userRepository, $manager);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ApplicationController::class);
    }

    function it_returns_the_correct_response_on_get_when_the_user_is_found(ServerRequestInterface $serverRequest, Item $item, Scope $scope)
    {
        $serverRequest->getAttribute('id')->willReturn(999);
        $this->userRepository->getById(999)->willReturn($item);
        $this->manager->createData($item)->willReturn($scope);
        $scope->toArray()->willReturn(['id'=>999]);

        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Content-Type' => 'json',
        ];
        $fakeResponse = new JsonResponse(['id'=>999], 200, $headers);

        $this->get($serverRequest)->shouldReturnAnInstanceOf(JsonResponse::class);
        $realResponse = $this->get($serverRequest);

        $this->assertResponsesAreEqual($realResponse,$fakeResponse);
    }

    function it_throws_on_get_when_the_user_is_not_found(ServerRequestInterface $serverRequest)
    {
        $serverRequest->getAttribute('id')->willReturn('not_valid');
        $this->userRepository->getById('not_valid')->willReturn(false);

        $this->shouldThrow(ResourceNotFound::class)->during('get',[$serverRequest]);
    }

    function it_returns_the_correct_response_on_getList_when_the_users_are_found(ServerRequestInterface $serverRequest, Collection $collection, Scope $scope)
    {
        $this->userRepository->getAll()->willReturn($collection);
        $this->manager->createData($collection)->willReturn($scope);
        $scope->toArray()->willReturn([['id'=>999],['id'=>998]]);

        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Content-Type' => 'json',
        ];
        $fakeResponse = new JsonResponse([['id'=>999],['id'=>998]], 200, $headers);

        $this->getList($serverRequest)->shouldReturnAnInstanceOf(JsonResponse::class);
        $realResponse = $this->getList($serverRequest);

        $this->assertResponsesAreEqual($realResponse,$fakeResponse);
    }

    function assertResponsesAreEqual($real,$fake)
    {
        $real->getHeaders()->shouldEqual($fake->getHeaders());
        $real->getStatusCode()->shouldEqual($fake->getStatusCode());
        $real->getBody()->getContents()->shouldEqual($fake->getBody()->getContents());
    }

}
