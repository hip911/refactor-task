<?php

namespace spec\Refactor\Application\Transformer;

use Refactor\Application\Model\User;
use Refactor\Application\Transformer\UserTransformer;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserTransformerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserTransformer::class);
    }

    function it_can_restructure_the_resource(User $user)
    {
        $this->transform($user);
        $user->getId()->shouldBeCalled();
        $user->getFirstName()->shouldBeCalled();
        $user->getSecondName()->shouldBeCalled();
    }
}
