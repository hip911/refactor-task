<?php

require_once __DIR__ . '/../vendor/autoload.php';
$config = require_once '../config/config.php';

set_exception_handler(function (\Exception $e) {
    $response = (new \Zend\Diactoros\Response\JsonResponse(['error' => $e->getMessage()],500));
    shutdown($response);
});

require_once '../config/dependencies.php';
require_once '../config/routes.php';

/** @var \Psr\Http\Message\ResponseInterface $response */

function shutdown($response) {
    http_response_code($response->getStatusCode());
    foreach ($response->getHeaders() as $name => $values) {
        foreach ($values as $value) {
            header(sprintf('%s: %s', $name, $value), false);
        }
    }
    echo $response->getBody();
}

register_shutdown_function(function() use ($response) {
    call_user_func('shutdown',$response);
});
