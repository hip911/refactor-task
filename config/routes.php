<?php
use Aura\Router\RouterContainer;

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();
$map->options('catchall','{/controller,action,id}',function($request) {
    $headers = [
        'Access-Control-Allow-Methods' => 'GET',
        'Content-Type' => 'json',
    ];
    return (new \Zend\Diactoros\Response\EmptyResponse(200, $headers));
});
$map->get('users.getList', '/index.php', \Refactor\Application\Controller\ApplicationController::class.'::getList');
$map->get('users.get', '/index.php/{id}', \Refactor\Application\Controller\ApplicationController::class.'::get');

$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);
if (! $route) {
    echo "No route found for the request.";
    exit;
}

foreach ($route->attributes as $key => $val) {
    $request = $request->withAttribute($key, $val);
}
$request = $request->withHeader('Content-Type','json');
$request = $request->withHeader('Access-Control-Allow-Origin','*');

$handler = $route->handler;
if($handler instanceof Closure) {
    $response = $handler($request);
} else{
    list($object,$method) = explode('::',$handler);
    $response = call_user_func_array([$injector->make($object),$method],[$request]);
}
