<?php

$injector = new Auryn\Injector;
$injector->defineParam('csvPath', __DIR__ . $config['users_csv_path']);
$injector->alias(\Refactor\Application\Repository\UserRepository::class,\Refactor\Application\Repository\CsvUserRepository::class);